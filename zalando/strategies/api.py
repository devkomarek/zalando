from rest_framework import viewsets

from zalando.strategies.models import Strategy
from zalando.strategies.serializers import StrategySerializer


class StrategyViewSet(viewsets.ModelViewSet):
    queryset = Strategy.objects.all()
    serializer_class = StrategySerializer
