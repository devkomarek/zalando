from django.urls import path, include
from rest_framework import routers

from zalando.strategies.serializers import StrategySerializer

events_router = routers.DefaultRouter()
events_router.register('', StrategySerializer, 'strategies')

urlpatterns = [
    path('strategies/', include(events_router.urls)),
]
