from django.urls import path, include
from rest_framework import routers

from zalando.tasks.api import TaskViewSet

events_router = routers.DefaultRouter()
events_router.register('', TaskViewSet, 'tasks')

urlpatterns = [
    path('tasks/', include(events_router.urls)),
]
