import pika
import requests
from django.conf import settings
from urllib3 import HTTPConnectionPool

from zalando.accounts.serializers import AccountSerializer
from zalando.bots.models import Bot
from zalando.bots.serializers import BotSerializer
from zalando.tasks.models import Task
from zalando.tasks.serializers import TaskSerializer


class Connection:
    PARENT_BOT_QUEUE_NAME = 'parent_bot'
    BOT_PARENT_QUEUE_NAME = 'bot_parent'

    def __init__(self):
        credential = pika.PlainCredentials(username=settings.RABBIT_USER, password=settings.RABBIT_PASSWORD)
        param = pika.ConnectionParameters(host=settings.RABBIT_HOST_IP, port=settings.RABBIT_PORT, credentials=credential)
        connection = pika.BlockingConnection(param)
        self.parent_bot = connection.channel()
        self.parent_bot.queue_declare(queue=self.PARENT_BOT_QUEUE_NAME)
        self.parent_bot.basic_qos(prefetch_count=1)
        self.bot_parent = connection.channel()
        self.bot_parent.queue_declare(queue=self.BOT_PARENT_QUEUE_NAME)
        self.bot_parent.basic_qos(prefetch_count=1)

    def send_task(self, task):
        self.parent_bot.basic_publish(exchange='', routing_key=self.PARENT_BOT_QUEUE_NAME, body=task)

    def accept_task(self, task_id):
        bot = Bot.objects.first()
        session = requests.Session()
        session.headers.update({'uuid': bot.uuid})
        return session.patch(settings.BASE_URL + '/tasks/' + str(task_id))

    def listen_on_task(self, callback):
        self.parent_bot.basic_consume(auto_ack=False, on_message_callback=callback, queue=self.PARENT_BOT_QUEUE_NAME)
        self.parent_bot.start_consuming()

    def register(self, bot):
        try:
            post = requests.post(settings.BASE_URL + '/bots/', data=BotSerializer(bot).data)
        except HTTPConnectionPool:
            raise Exception('Bot have problem with connection to the parent site')
        return post

    def reserve_account(self):
        bot = Bot.objects.first()
        session = requests.Session()
        session.headers.update({'uuid': bot.uuid})
        response = session.get(settings.BASE_URL + '/accounts/?available')
        accounts = AccountSerializer(response, many=True).data
        if accounts:
            account_id = next(accounts).id
            return session.put(settings.BASE_URL + '/accounts/' + str(account_id) + '/', data={'inUse:True'})
        return None
