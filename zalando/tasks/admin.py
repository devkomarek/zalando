from django.contrib import admin

from zalando.tasks.models import Task

admin.site.register(Task)
