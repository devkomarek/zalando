import os

import uuid
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from zalando.bots.models import Bot
from zalando.bots.services.shoppingbot import ShoppingBot


class Task(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    name = models.CharField(max_length=100, null=True)
    strategy = models.ForeignKey('strategies.Strategy', on_delete=models.SET_NULL, null=True)

    @classmethod
    def lookup_instance(cls, uuid, **kwargs):
        try:
            return Task.objects.get(uuid=uuid)
        except Task.DoesNotExist:
            print('Task is not exist')


@receiver(post_save, sender=Task)
def new_task_incoming_execute(sender, **kwargs):
    if bool(os.environ.get('BOT_MODE')):
        task = sender.objects.first()
        service = ShoppingBot(task, Bot.objects.first())
