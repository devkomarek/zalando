import os
import sys

from django.apps import AppConfig


class ZalandoConfig(AppConfig):
    name = 'zal'
