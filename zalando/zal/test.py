import namesgenerator
from django.test import TestCase
# Create your tests here.
from rest_framework.test import APIRequestFactory

from zalando.tasks.models import Task
from zalando.tasks.serializers import TaskSerializer


class GoogleTest(TestCase):
    factory = APIRequestFactory()

    def test_send_message_task_to_parent_bot_queue(self):
        import pika
        credential = pika.PlainCredentials(username='zalando', password='zal')
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='35.246.241.55', port='5672', credentials=credential))
        channel = connection.channel()
        channel.queue_declare(queue='parent_bot')
        task = Task(name=namesgenerator.get_random_name())
        s = TaskSerializer(data=task.__dict__)
        s.is_valid()
        channel.basic_publish(exchange='',
                              routing_key='parent_bot',
                              body=s.data.__str__())
        connection.close()

    def test_connection_configuration_to_technical_user2(self):
        def callback(ch, method, properties, body):
            print(" [x] Received %r" % body)
            connection.close()
        import pika
        credential = pika.PlainCredentials(username='zalando', password='zal')
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='35.246.241.55', port='5672', credentials=credential))
        channel = connection.channel()
        channel.queue_declare(queue='hello')
        channel.basic_consume(queue='hello',
                              auto_ack=True,
                              on_message_callback=callback)
        channel.start_consuming()
        connection.close()
