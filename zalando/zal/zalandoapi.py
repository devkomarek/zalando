import pandas as pd
import pause
import requests


class ZalandoApi:
    headers = {'Content-type': 'application/json; charset=utf-8', 'Accept': 'application/json, text/plain, */*',
               'Accept-Encoding': 'gzip, deflate, br', 'X-Requested-With': 'XMLHttpRequest'}
    user_id = None

    def __init__(self, cookies):
        self.session = requests.Session()
        cookies_args = [{'name': x.split('=')[0].replace(' ', ''), 'value': x.split('=')[1].replace(' ', '')} for x in
                        cookies.split(';')]
        self.user_id = str(cookies.split(';')[0])
        for c in cookies_args:
            self.session.cookies.set(**c)

    def refresh_cart(self):
        URL = 'https://www.zalando-lounge.pl/api/checkout/cart'
        return self.session.put(URL, headers=self.headers)

    def count_items_in_cart(self):
        URL = 'https://www.zalando-lounge.pl/api/checkout/cart'
        response = self.session.get(URL, headers=self.headers)
        return len(response.json()['cartModel']['cartItems'])

    def put_item_to_cart(self, config_sku, simple_sku, campaign_id):
        URL = 'https://www.zalando-lounge.pl/api/checkout/cart/items'
        data = '{"configSku":"<co>","simpleSku":"<sim>","additional":{"reco":0},"campaignIdentifier":"<cam>"}' \
            .replace('<co>', config_sku) \
            .replace('<sim>', simple_sku) \
            .replace('<cam>', campaign_id)
        response = self.session.put(URL, headers=self.headers, data=data)
        return response.json()

    def retrieve_all_data(self):
        url = 'https://www.zalando-lounge.pl/event'
        return self.session.get(url, headers=self.headers).json()['initialState']

    def retrieve_articles_from_campaigns(self, campaigns):
        URL = 'https://www.zalando-lounge.pl/api/campaigns/<campaign>/articles?page='
        articles = []
        attempts = 5
        for a in range(attempts):
            for i, campaign in enumerate(campaigns):
                print('Current_' + str(i) + ': ' + campaign[0])
                page = 1
                while True:
                    response = self.session.get(URL.replace('<campaign>', campaign[0]) + str(page),
                                                headers=self.headers)
                    try:
                        article = response.json()
                        articles.extend(article['results'])
                        if len(article.get('results')) < 1:
                            break
                    except Exception:
                        print('Something wrong!')
                        print(response.content)
                    page += 1
            if not articles:
                pause.seconds(1)
            else:
                break
        return pd.DataFrame.from_dict(articles)
