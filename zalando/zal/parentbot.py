from datetime import date

from zalando.accounts.models import Account
from zalando.campaigns.models import Campaign
from zalando.zal.zalandoapi import ZalandoApi


class ParentBot:
    def __init__(self):
        self.queryset = Campaign.objects
        cookies = Account.objects.get(name='Parent').cookies
        self.api = ZalandoApi(cookies=cookies)

    def fetch_resources(self):
        data = self.api.retrieve_all_data()
        self.queryset.create(data=data, fetch_day=date.today())
