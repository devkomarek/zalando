from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path, include, reverse_lazy
from django.views.generic.base import RedirectView
from rest_framework import permissions, authentication

admin.site.site_header = "zalando Admin"
admin.site.site_title = "zalando Admin site"
admin.site.index_title = "Welcome to zalando v1 Admin site"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('zalando.accounts.urls')),
    path('api/', include('zalando.bots.urls')),
    path('api/', include('zalando.campaigns.urls')),
    # the 'api-root' from django rest-frameworks default router
    # http://www.django-rest-framework.org/api-guide/routers/#defaultrouter
    re_path(r'^$', RedirectView.as_view(url=reverse_lazy('api-root'), permanent=False)),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
          path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns

    # Auto generating docs
    from drf_yasg.views import get_schema_view
    from drf_yasg import openapi


    schema_view = get_schema_view(
       openapi.Info(
          title="zalando API",
          default_version='v1',
          description="Welcom in zalando docs",
          terms_of_service="https://www.google.com/policies/terms/",
          contact=openapi.Contact(email="lukasz.szymszon@apilia.pl"),
       ),
       public=True,
       permission_classes=(permissions.AllowAny,),
       authentication_classes=(authentication.BasicAuthentication,),
    )

    urlpatterns = [
       url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
       url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
       url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    ] + urlpatterns
