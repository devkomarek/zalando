"""
WSGI config for viral project.
It exposes the WSGI callable as a module-level variable named ``application``.
For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/gunicorn/
"""
import os


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "zalando.config")
os.environ.setdefault("DJANGO_CONFIGURATION", "Dev")

from configurations.wsgi import get_wsgi_application
application = get_wsgi_application()

def ready():
    if int(os.environ.get('BOT_MODE')) == 1:
        from zalando.bots.services.parentslave import ParentSlave
        from zalando.bots.models import Bot
        service = ParentSlave()
        service.register_bot()
        bot = Bot.objects.first()
        bot.published()
        service.listen_on_task()

ready()