from rest_framework import serializers

from zalando.campaigns.models import Campaign


class UpcomingCampaignSerializer(serializers.Serializer):
    def to_representation(self, instance):
        repr = {}
        mylounge = instance.data['mylounge']
        campaigns = mylounge['upcoming']['campaigns']
        for campaign in campaigns.values():
            repr[campaign['campaignIdentifier']] = {
                'name': campaign['name'],
                'maxDiscount': campaign['maxDiscount'],
                'dateStart': campaign['dateStart'],
                'dateEnd': campaign['dateEnd'],
                'images': campaign['images']['1x1']['highres']
            }
        return repr

    class Meta:
        model = Campaign
        fields = '__all__'


class CampaignSerializer(serializers.ModelSerializer):
    class Meta:
        model = Campaign
        fields = ['fetch_day']
