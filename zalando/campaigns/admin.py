from django.contrib import admin

from zalando.campaigns.models import Campaign

admin.site.register(Campaign)
