from datetime import date

from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from zalando.campaigns.serializers import CampaignSerializer, UpcomingCampaignSerializer
from .models import Campaign


class CampaignViewSet(viewsets.ModelViewSet):
    queryset = Campaign.objects.filter(fetch_day=date.today())
    serializer_class = CampaignSerializer

    @action(methods=['GET'], detail=False)
    def upcoming(self, request):
        return Response(UpcomingCampaignSerializer(self.queryset.first()).data)
