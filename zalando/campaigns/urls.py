from django.urls import path, include
from rest_framework import routers

from zalando.campaigns.api import CampaignViewSet

events_router = routers.DefaultRouter()
events_router.register('', CampaignViewSet, 'campaign')

urlpatterns = [
    path('campaigns/', include(events_router.urls)),
]
