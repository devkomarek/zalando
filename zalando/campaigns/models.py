from django.contrib.postgres.fields import JSONField
from django.db import models


class Campaign(models.Model):
    fetch_day = models.DateField()
    data = JSONField()
