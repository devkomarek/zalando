import os

from environ import environ

from .common import Common

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class Dev(Common):
    DEBUG = True
    ALLOWED_HOSTS = ["*"]
    INTERNAL_IPS = [
        '127.0.0.1', 'localhost', '0.0.0.0'
    ]
    INSTALLED_APPS = Common.INSTALLED_APPS
    MIDDLEWARE = Common.MIDDLEWARE

    INSTALLED_APPS += ('debug_toolbar',)
    INSTALLED_APPS += ('drf_yasg',)
    # Static files (CSS, JavaScript, Images)
    # https://docs.djangoproject.com/en/2.0/howto/static-files/
    # http://django-storages.readthedocs.org/en/latest/index.html
    MIDDLEWARE += ('debug_toolbar.middleware.DebugToolbarMiddleware',)
    MIDDLEWARE += ('zalando.disablecsrf.DisableCSRF',)

    CORS_ORIGIN_ALLOW_ALL = True

    # Django Rest Framework
    REST_FRAMEWORK = Common.REST_FRAMEWORK
