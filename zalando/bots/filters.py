from datetime import datetime

from dateutil.parser import parse
from django.db.models import Max, Min
from rest_framework import filters


class EventSlotTimeFilterBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        start = parse(request.query_params.get('start', str(datetime.min)))
        end = parse(request.query_params.get('end', str(datetime.max)))
        return queryset.annotate(Min('slots__slot')).annotate(Max('slots__slot')).filter(
            slots__slot__min__gt=start).filter(slots__slot__max__lt=end).order_by('-created')
