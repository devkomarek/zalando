from rest_framework import viewsets

from zalando.bots.serializers import BotSerializer
from .models import Bot


class BotViewSet(viewsets.ModelViewSet):
    queryset = Bot.objects.all()
    serializer_class = BotSerializer
