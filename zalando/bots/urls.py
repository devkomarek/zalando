from django.urls import path, include
from rest_framework import routers

from zalando.bots.api import BotViewSet

events_router = routers.DefaultRouter()
events_router.register('', BotViewSet, 'bots')

urlpatterns = [
    path('bots/', include(events_router.urls)),
]
