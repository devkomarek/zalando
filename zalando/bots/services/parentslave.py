import ast

import namesgenerator
from zalando.accounts.serializers import AccountSerializer
from zalando.bots.models import Bot
from zalando.tasks.connection import Connection
from zalando.tasks.models import Task
from zalando.tasks.serializers import TaskSerializer


class ParentSlave:
    queryset = Bot.objects.all()

    def __init__(self):
        self.connection = Connection()

    def register_bot(self):
        name = namesgenerator.get_random_name()
        bot = self.queryset.create(name=name)
        # TODO when register not working try repeat
        response = self.connection.register(bot)
        bot.register()
        bot.save()
        return response

    def reserve_account(self):
        account = self.connection.reserve_account()
        AccountSerializer(account).save()

    def listen_on_task(self):
        def callback(ch, method, properties, body):
            if Task.objects.all().exists():
                # TODO use fsm
                # TODO what if task is executed but bot get new task
                import time
                time.sleep(5)
                ch.basic_nack(delivery_tag=method.delivery_tag)
                return None
            serializer = TaskSerializer(data=ast.literal_eval(body.decode()))
            serializer.is_valid(raise_exception=True)
            serializer.save()

        self.connection.listen_on_task(callback)
