import uuid as uuid
from django.db import models
from django_fsm import FSMField, transition


class Bot(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    name = models.CharField(max_length=100)
    state = FSMField(default='initial', verbose_name='Bot state')

    @transition(field=state, source='initial', target='ready_for_task')
    def registration(self):
        print('reg')

    @transition(field=state, source='ready_for_task', target='prepare_to_task')
    def new_task(self):
        print('pub')

    @transition(field=state, source='prepare_to_task', target='wait')
    def ready_for_shopping(self):
        print('pub')

    @transition(field=state, source='wait', target='shopping')
    def near_to_shopping(self):
        print('pub')

    @transition(field=state, source='shopping', target='save_data')
    def executed_whole_task(self):
        print('pub')

    @transition(field=state, source='save_data', target='ready_for_task')
    def complete_whole_task(self):
        print('pub')

    @classmethod
    def lookup_instance(cls, uuid, **kwargs):
        try:
            return Bot.objects.get(uuid=uuid)
        except Bot.DoesNotExist:
            print('Bot is not exist')
