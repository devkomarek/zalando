from django.urls import path, include
from rest_framework import routers

from zalando.accounts.api import AccountViewSet

events_router = routers.DefaultRouter()
events_router.register('', AccountViewSet, 'accounts')

urlpatterns = [
    path('accounts/', include(events_router.urls)),
]
