from rest_framework import viewsets

from zalando.accounts.filters import AvailableAccounts
from zalando.accounts.serializers import AccountSerializer
from .models import Account


class AccountViewSet(viewsets.ModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    filter_backends = [AvailableAccounts]
