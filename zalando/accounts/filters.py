from datetime import datetime

from dateutil.parser import parse
from django.db.models import Max, Min
from rest_framework import filters


class AvailableAccounts(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if request.query_params.get('available') is not None:
            return queryset.filter(in_use=False)
        return queryset

