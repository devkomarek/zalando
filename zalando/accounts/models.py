from django.db import models


class Account(models.Model):
    name = models.CharField(max_length=100, null=True, unique=True)
    cookies = models.CharField(max_length=5000, unique=False)
    in_use = models.BooleanField(default=False)
