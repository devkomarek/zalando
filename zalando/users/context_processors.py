from decouple import config
from django.conf import settings


def social_app_keys(request):
    return {
        'googleoauth2_key': config('SOCIAL_AUTH_GOOGLE_OAUTH2_KEY'),
    }