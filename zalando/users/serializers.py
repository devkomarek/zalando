from rest_framework import serializers

from .models import User


class UserSerializer(serializers.ModelSerializer):

    def to_representation(self, obj):
        return {
            'username': obj.username,
            'email': obj.email
        }

    class Meta:
        model = User
        fields = ('pk',)


class CreateUserSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        # call create_user on user object. Without this
        # the password will be stored in plain text.
        user = User.objects.create_user(**validated_data)
        return user

    class Meta:
        model = User
        fields = ('sub', 'username', 'password', 'first_name', 'last_name', 'email')
        extra_kwargs = {'password': {'write_only': True}}
