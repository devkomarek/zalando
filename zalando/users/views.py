from django.views.generic.detail import BaseDetailView
from rest_framework.authentication import SessionAuthentication, TokenAuthentication


class UserSessionDetailView(BaseDetailView):
    authentication_classes = (SessionAuthentication,)


class UserTokenDetailView(BaseDetailView):
    authentication_classes = (TokenAuthentication,)

from django.shortcuts import render


def home(request):
    return render(request, 'home.html')

