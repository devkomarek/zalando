#!/usr/bin/env bash
find . -path "*/migrations/*.py" -not -name "__init__.py" -not -path "./venv/*" -delete
rm db.sqlite3
DJANGO_CONFIGURATION=Dev python manage.py makemigrations --settings="xchangerooms.config.dev"
DJANGO_CONFIGURATION=Dev python manage.py migrate --settings="xchangerooms.config.dev"
DJANGO_CONFIGURATION=Dev python manage.py loaddata fixtures/* --settings="xchangerooms.config.dev"
