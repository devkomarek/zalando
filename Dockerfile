FROM gcr.io/google-appengine/python
ENV PYTHONUNBUFFERED 1

# Allows docker to cache installed dependencies between builds
RUN virtualenv /env -p python3.7
ENV PATH=/env/bin:$PATH
RUN pip install --upgrade pip
COPY ./requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

# Adds our application code to the image
COPY . code
WORKDIR code

EXPOSE 8000

# Migrates the database, uploads staticfiles, and runs the production server
CMD python ./manage.py migrate && \
    gunicorn -w 4 --bind 0.0.0.0:$PORT --access-logfile - xchangerooms.wsgi:application
