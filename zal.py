from datetime import datetime

import pandas as pd
import pause
import requests
from click._unicodefun import click


@click.group()
def app():
    click.echo('Welcome in Zal app! We run for you command:')


@app.command()
@click.option('--cookies', default=None, help='cookies from user account')
@click.option('--filters', default=['38'], help='defined filters for bot strategies')
@click.option('--ordering', default=['different', 'savings'], help='defined filters for bot strategies')
@click.option('--campaign', default=[], help='customize your list')
def shopping(cookies, filters, ordering, campaign):
    click.echo('Shopping time!')
    Zal(cookies, filters, ordering, campaign).wait()


@app.command()
@click.option('--cookies', default=None, help='cookies from user account')
def get_all_open(cookies):
    click.echo('Get open please!')
    Zal(cookies).reload_data().print_open()


@app.command()
@click.option('--closest', default=False, help='get nearest upcoming campaign')
@click.option('--cookies', default=None, help='cookies from user account')
def get_all_upcoming(cookies, closest):
    click.echo('Get upcoming please!')
    if closest:
        Zal(cookies).reload_data().print_closest()
    else:
        Zal(cookies).reload_data().print_upcoming()


class Zal:
    headers = {'Content-type': 'application/json; charset=utf-8', 'Accept': 'application/json, text/plain, */*',
               'Accept-Encoding': 'gzip, deflate, br', 'X-Requested-With': 'XMLHttpRequest'}
    session = requests.Session()
    size_cart = 30

    def __init__(self, cookies, filters=None, ordering=None, campaign=None):
        self.user_campaign = campaign
        self.filters = filters
        self.ordering = ordering
        if cookies:
            self.cookies = cookies
        else:
            import cookies
            self.cookies = cookies.cookies
        self.data = {}
        self.upcomping = {}
        self.open = {}
        self.today = datetime.now().timestamp()
        self.drop_start = 0
        self.drop_items = {}
        cookies_args = [{'name': x.split('=')[0].replace(' ', ''), 'value': x.split('=')[1].replace(' ', '')} for x in
                        self.cookies.split(';')]
        for c in cookies_args:
            self.session.cookies.set(**c)

    def print_closest(self):
        self.gets_event_closest()
        print(str(datetime.fromtimestamp(self.drop_start / 1000)))
        print(list(map(lambda x: x[0], self.drop_items)))

    def print_open(self):
        for x in self.open.keys():
            print(str(x))
            print(self.open[x]['name'])
            print(str(datetime.fromtimestamp(self.open[x]['dateStart'] / 1000)))
            print('=*=')

    def print_upcoming(self):
        for x in self.upcomping.keys():
            print(str(x))
            print(self.upcomping[x]['name'])
            print(str(datetime.fromtimestamp(self.upcomping[x]['dateStart'] / 1000)))
            print('=*=')

    def reload_data(self):
        url = 'https://www.zalando-lounge.pl/event'
        self.data = self.session.get(url, headers=self.headers).json()['initialState']
        self.upcomping = self.data['mylounge']['upcoming']['events']
        self.open = self.data['mylounge']['open']['events']
        return self

    def gets_event_closest(self):
        closest = list(map(lambda x: (x, self.upcomping[x]['dateStart']), self.upcomping.keys()))
        self.drop_start = min(map(lambda x: x[1], closest))
        self.drop_items = [x for x in closest if x[1] == self.drop_start and 'ZZLS6X' in x]

    def refresh_cart(self):
        URL = 'https://www.zalando-lounge.pl/api/checkout/cart'
        self.session.put(URL, headers=self.headers)

    def count_items_in_cart(self):
        URL = 'https://www.zalando-lounge.pl/api/checkout/cart'
        response = self.session.get(URL, headers=self.headers)
        return len(response.json()['cartModel']['cartItems'])

    def put_items_to_cart(self, articles):
        if articles is None:
            print('Put 0 items to cart')
            return
        size_cart = self.size_cart
        for _, x in articles.iterrows():
            response = self.put_item_to_cart(x['configSku'], x['sku'], x['campaignIdentifier'])
            print('Put item: ' + str(response))
            if size_cart <= 0:
                if self.count_items_in_cart() >= self.size_cart:
                    break
            size_cart -= size_cart - 1

    def put_item_to_cart(self, config_sku, simple_sku, campaign_id):
        URL = 'https://www.zalando-lounge.pl/api/checkout/cart/items'
        data = '{"configSku":"<co>","simpleSku":"<sim>","additional":{"reco":0},"campaignIdentifier":"<cam>"}'.replace('<co>',config_sku).replace('<sim>',simple_sku).replace('<cam>',campaign_id)
        response = self.session.put(URL, headers=self.headers, data=data)
        print('I put:')
        print(config_sku,simple_sku,campaign_id)
        return response.json()

    def get_articles_from_campaign(self, campaigns):
        print('Scrapping articles from campaigns')
        URL = 'https://www.zalando-lounge.pl/api/campaigns/<campaign>/articles?page='
        articles = []
        attempts = 5
        for a in range(attempts):
            for i, campaign in enumerate(campaigns):
                print('Current_' + str(i) + ': ' + campaign[0])
                page = 1
                while True:
                    response = self.session.get(URL.replace('<campaign>', campaign[0]) + str(page),
                                                headers=self.headers)
                    try:
                        article = response.json()
                        articles.extend(article['results'])
                        if len(article.get('results')) < 1:
                            break
                    except Exception:
                        print('Something wrong!')
                        print(response.content)
                    page += 1
            if not articles:
                print('ss')
            else:
                break
        return pd.DataFrame.from_dict(articles)

    def get_best_items(self, articles):
        link_to_article = 'https://www.zalando-lounge.pl/campaigns/<camp>/categories/<cat>/articles/<config-sku>'
        articles = articles.drop_duplicates('sku')
        articles = articles[articles.stockStatus == 'AVAILABLE']
        articles = articles.drop(
            columns=['attributes', 'brandCode', 'images', 'media', 'subtitle', 'urlPath', 'hasSimilar', 'gender',
                     'stockStatus'])
        df_open = articles.rename(columns={'sku': 'configSku'})

        def check_38_to_44(simples):
            for s in simples:
                if s['stockStatus'] == 'AVAILABLE':
                    return True
            return False

        def get_38_to_44(simples):
            for s in simples:
                if s['stockStatus'] == 'AVAILABLE':
                    s['different'] = s['price'] - s['specialPrice']
                    return s

        strategy_articles = {i: pd.io.json.json_normalize(get_38_to_44(x)) for i, x in df_open.pop('simples').items() if
                             check_38_to_44(x)}
        if not strategy_articles:
            print("All items do not match to our strategy")
            return None
        articles = (pd.concat(strategy_articles).reset_index(level=1, drop=True).join(df_open).reset_index(drop=True))
        articles = articles.drop(columns=['stockStatus'])
        articles = articles.sort_values(by=self.ordering, ascending=False)

        def prepare_link(row):
            return link_to_article.replace('<camp>', str(row['campaignIdentifier'])).replace('<cat>', str(
                row['categoryId'])).replace('<config-sku>', str(row['configSku']))

        articles['link_to_article'] = articles.apply(prepare_link, axis=1)
        return articles

    def wait(self):
        print("Getting data")
        self.reload_data()
        self.gets_event_closest()
        print("I wait for drop that begin on " + str(datetime.fromtimestamp(self.drop_start / 1000)))
        print("Articles on the target: ")
        print(str(self.user_campaign) if self.user_campaign else str(self.drop_items))
        pause.until(datetime.fromtimestamp(self.drop_start / 1000))
        print('Attack begin ......')
        articles = self.get_articles_from_campaign(self.user_campaign if self.user_campaign else self.drop_items)
        print('I have articles')
        print('Time to get the best')
        articles = self.get_best_items(articles)
        self.put_items_to_cart(articles)
        print('I done!')
        print('Check my items for user:')
        print(str(self.cookies.split(';')[0]))
        while True:
            print("I am waiting! For my rest of live!")
            self.refresh_cart()
            pause.minutes(10)


if __name__ == '__main__':
    app()
